## About
A small scraper for `https://developers.google.com/android/images#razorg` downloading image files and keeping track of their metadata in mongoDB.
Hacked together as a code example.     

## Running
Make sure you have a MongoDB instance available.
Prepare a configuration file. Look at `config/dev-config.json` for reference. The fields should be self-explanatory. 

You can override some of the configuration using command line flags. (Run the script or executable with `-h` for mor info)

### As a python script
Run `src/scraper.py path/to/your/config.json` after installing the required packages from `requirements.txt`. 

### As an executable
You can build an executable from this python script using [pyinstaller](http://www.pyinstaller.org/).
If you have docker installed, the [dev_build service](#dev_build) builds an executable for linux.

## Docker-compose
The docker-compose file describes three services used to develop / build and test the scraper. To launch them, run: `docker-compose up <service>`
### dev_database
Launches a standard mongoDB instance which you can use while developing the application.

### dev_build
Builds the scraper into a linux executable using pyinstaller. 
The results will be stored in `./dist/linux/scraper`. Note: You need to keep the files in the directory together if you want to run the actual executable. (`./dist/linux/scraper/scraper`). 

### test
Runs the executable built by the `dev_build` service with `config/test-config.josn`.     
