import re
import sys


def get_image_links(soup):
    attributes = {"data-category": "Android Images"}
    links = soup.find_all("a", **attributes)
    return links


class Image:
    """
    Represents an image listed on the site.
    Metadata is scraped from the site when the corresponding properties are accessed.
    """

    def __init__(self, link):
        """
        :param link: <a> tag representing the download link on the site.
        """
        self._link = link

    @property
    def url(self):
        return self._link.get("href")

    @property
    def code_name(self):
        data_label = self._link["data-label"]
        return re.match("(.*) for (.*) \\[(.*)]", data_label).group(1)

    @property
    def device(self):
        data_label = self._link["data-label"]
        return re.match("(.*) for (.*) \\[(.*)]", data_label).group(2)

    @property
    def version_id(self):
        data_label = self._link["data-label"]
        return re.match("(.*) for (.*) \\[(.*)]", data_label).group(3)

    @property
    def pretty_version(self):
        row = self._link.find_parent("tr")
        version_cell = row.find("td", string=re.compile("^\\d+\\.\\d+\\.?\\d*.*\\(.*\\)$"))
        return version_cell.text

    @property
    def android_version(self):
        return re.match("^\\d+\\.\\d+\\.?\\d*", self.pretty_version).group(0)

    @property
    def sha_256_checksum(self):
        row = self._link.find_parent("tr")
        checksum_cell = row.find("td", string=re.compile("^[A-Fa-f0-9]{64}$"))
        return checksum_cell.text


def safe_get_from_image(image, properties, sensitive):
    """
    Used to extract meta data from Image objects into a map, while suppressing exceptions when sensitive is false.
    :param image: Image object
    :param properties: properties to safely extract
    :param sensitive: If false, exceptions occurring when scraping metadata will be suppressed.
    The corresponding meta field will contain a string "Scrape failed".
    :return: Map containing containing the metadata.
    """
    result = {}
    for prop in properties:
        try:
            result[prop] = getattr(image, prop)
        except Exception as e:
            print("Failed to scrape {} for image: {}".format(prop, image.url), file=sys.stderr)
            result[prop] = "Scrape failed"
            if sensitive:
                raise e

    return result
