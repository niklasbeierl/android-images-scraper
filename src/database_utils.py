import time


def get_image_key(image):
    """
    Get the key used to access an image in the images collection.
    :param image: an Image object.
    :return: key as String.
    """
    image_key = image.url.split("/")[-1]  # I trust google keeps the file names for the images unique.
    return image_key


def add_image(image_data, image_key, collection):
    now = time.time()
    collection.insert({
        "_id": image_key,
        "first_seen": now,
        "last_seen": now,
        "device": image_data["device"],
        "sha_256_checksum": image_data["sha_256_checksum"],
        "removed_from_site": False,
        "version": {
            "pretty": image_data["pretty_version"],
            "code_name": image_data["code_name"],
            "android": image_data["android_version"],
            "id": image_data["version_id"]
        },
        "file": {
            "url": image_data["url"]
        }
    })


def set_last_seen(image_key, last_seen, collection):
    collection.update_one({"_id": image_key}, {"$set": {"last_seen": last_seen, "deleted_from_site": False}})


def mark_image_not_on_site(image_key, collection):
    collection.update_one({"_id": image_key}, {"$set": {"removed_from_site": True}})


def set_file_path(image_key, path, collection):
    collection.update_one({"_id": image_key}, {"$set": {"file.local_path": path}})


def get_images_without_file_path(collection):
    images_without_file = collection.find({"file.local_path": {"$eq": None}}, {"file.url": 1})
    return [(image["file"]["url"], image["_id"]) for image in images_without_file]


def get_images_expected_on_site(collection):
    """
    Get ids of images in the database which are not marked as "removed_from_site".
    :param collection: Collection containing the images
    :return: List of image metadata documents (containing only _id).
    """
    return collection.find({"removed_from_site": False}, {"_id": 1})


def exits_in_collection(key, collection):
    return collection.count_documents({"_id": key}) != 0
