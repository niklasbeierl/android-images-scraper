import argparse
import json
import pymongo

description = """This script scrapes a website for android images. 
The images are downloaded and corresponding metadata is stored in mongodb."""
parser = argparse.ArgumentParser(description=description)
parser.add_argument("config",
                    type=argparse.FileType('r'),
                    nargs=1,
                    action="store",
                    help="path to configuration file")
parser.add_argument("--no-download",
                    action="store_const",
                    const=True,
                    help="set if you want to skip downloading the images")
parser.add_argument("--sensitive",
                    action="store_const",
                    const=True,
                    help="if set, the script exits when unable to obtain complete metadata for an image.")


def get_config():
    """
    Read config file and override settings based on command line arguments.
    :return: Map containing configuration.
    """
    args = parser.parse_args()
    config = json.load(args.config[0])

    if args.no_download:
        print("Downloads will be skipped.")
        config["download_disabled"] = True
    if args.sensitive:
        print("Script exits when unable to obtain complete metadata for an image.")
        config["sensitive"] = True

    return config


def connect_and_setup_db(db_config):
    """
    Connect to the database and select / create the relevant database and collection.
    :param db_config:
    :return: pymongo collection
    """
    client = pymongo.MongoClient(**db_config)
    db = client["android_images"]
    collection = db["images"]
    return collection
