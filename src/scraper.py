import os
from sys import exit
from shutil import copyfileobj

import requests
from bs4 import BeautifulSoup
import utils
from scrape_utils import *
from database_utils import *

IMAGE_PROPERTIES = ["version_id", "pretty_version", "android_version", "sha_256_checksum", "url", "device",
                    "code_name"]

config = utils.get_config()

download_disabled = bool(config.get("download_disabled"))
re_attempt_downloads = bool(config.get("re_attempt_downloads"))
sensitive = bool(config.get("sensitive"))
site_url = config["target"]["url"]

print("Connecting to database...")
image_collection = utils.connect_and_setup_db(config["database"])
print("connected.")

print("Getting document...")
site = requests.get(site_url)
print("Done.")

print("Parsing document and updating metadata...")
soup = BeautifulSoup(site.text.encode("utf8"), "html.parser")
image_links = get_image_links(soup)
images = [Image(image_link) for image_link in image_links]
image_keys = [get_image_key(image) for image in images]

to_download = get_images_without_file_path(image_collection) if re_attempt_downloads else []

for image_key, image in zip(image_keys, images):
    if not exits_in_collection(image_key, image_collection):
        image_data = safe_get_from_image(image, IMAGE_PROPERTIES, sensitive)
        add_image(image_data, image_key, image_collection)
        to_download.append((image.url, image_key))
    else:
        set_last_seen(image_key, time.time(), image_collection)

image_keys_on_site = set(image_keys)
image_keys_in_db = set([doc["_id"] for doc in get_images_expected_on_site(image_collection)])

for image_key in image_keys_in_db - image_keys_on_site:
    mark_image_not_on_site(image_key, image_collection)

if download_disabled:
    print("Exiting without downloading image files as specified in configuration or execution parameters.")
    exit(0)

download_dir = config["download"]["dir"]

for url, image_key in to_download:
    path = os.path.normpath(os.path.join(download_dir, image_key))
    abs_path = os.path.abspath(path)

    # probably a previous instance of this script still in the progress of downloading (files are huge)
    if os.path.isfile(abs_path):
        print("File {} already exists, but is not registered in database, skipping download.".format(abs_path),
              file=sys.stderr)
        continue

    download_sucessful = False
    try:
        file_handle = open(abs_path, "wb+")
        try:
            response = requests.get(url, stream=True, timeout=3)
            response.raise_for_status()
            print("Downloading {} to {}".format(url, abs_path))
            copyfileobj(response.raw, file_handle)
            set_file_path(image_key, abs_path, image_collection)
            download_sucessful = True
        except Exception:  # There is a lot to go wrong here...
            print("Downloading {} failed.".format(abs_path), file=sys.stderr)
        finally:
            file_handle.close()
            response.close()
    except (OSError, IOError):
        print("Unable to open file {}".format(abs_path), file=sys.stderr)
    finally:
        if not download_sucessful:
            try:
                os.remove(abs_path)
            except OSError:
                pass

image_collection.database.client.close()
print("Done")
exit(0)
